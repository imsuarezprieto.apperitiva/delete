package com.example.android.geoquiz


class Question(val questionsArray: Array<String>) {

    public var statement: String = ""
    public var answer: Boolean = false

    public fun random() {
        val entry = this.questions.entries.random()
        this.statement = entry.key
        this.answer = entry.value
    }

    private val questions: Map<String, Boolean> by lazy { parseResArray() }

    private fun parseResArray(): Map<String, Boolean> {
        val questionsMap = mutableMapOf<String, Boolean>()
        var stringSplitted: List<String>
        for (questionString in this.questionsArray) {
            stringSplitted = questionString.split("|")
            questionsMap.put(stringSplitted[0], if (stringSplitted[1] == "0") false else true)
        }
        return questionsMap
    }
}