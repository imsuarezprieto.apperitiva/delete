package com.example.android.geoquiz


import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.os.PersistableBundle
import android.view.ViewAnimationUtils
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity :
    AppCompatActivity() {

    //    lateinit var questions: Question

    private val viewModel: QuestionViewModel by viewModels()

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // SEE: https://developer.android.com/training/gestures/gesturenav
        this.window.navigationBarColor = Color.TRANSPARENT
        this.window.setDecorFitsSystemWindows(false)

        viewModel.statement.observe(this) {
                newStamentInt -> question_text.setText(newStamentInt) }

        true_button
            .setOnClickListener { this.checkAnswer(true) }
        false_button
            .setOnClickListener { this.checkAnswer(false) }
        next_fab
            .setOnClickListener { this.updateQuestion() }

    }

    private fun updateQuestion() {
        //        questions.random(
        this.viewModel.random()
        val cx = (next_fab.x + next_fab.width / 2).toInt()
        val cy = (next_fab.y + next_fab.height / 2).toInt()
        val rf = Math
            .hypot(mainActivity_content.width.toDouble(), mainActivity_content.height.toDouble())
            .toFloat()
        val anim = ViewAnimationUtils.createCircularReveal(mainActivity_content, cx, cy, 0f, rf)
        mainActivity_content.setBackgroundColor(
            ResourcesCompat.getColor(
                resources,
                R.color.colorAccent,
                null
            )
        )
        question_text.setText(this.viewModel.statement.value!!)
        (next_fab.drawable as Animatable).start()
        anim.duration = 500
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationEnd(animation)
                ValueAnimator
                    .ofArgb(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.colorAccent,
                            null
                        ),
                        ResourcesCompat.getColor(
                            resources,
                            android.R.color.background_light,
                            null
                        )
                    )
                    .apply {
                        duration = 500
                        start()
                        addUpdateListener { updatedAnimation ->
                            mainActivity_content.setBackgroundColor(updatedAnimation.animatedValue as Int)
                        }
                    }
            }

            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                question_text_an.setText(this@MainActivity.viewModel.statement.value!!)
            }
        })
        anim.start()
    }

    private fun checkAnswer(userAnswer: Boolean) {
        val correctAnswer = resources.getBoolean(this.viewModel.answer.value!!)
        if (userAnswer == correctAnswer)
            Snackbar
                .make(this.mainActivity_view, R.string.correct_toast, Snackbar.LENGTH_SHORT)
                .setGestureInsetBottomIgnored(true)
                .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                    override fun onDismissed(
                        transientBottomBar: Snackbar?,
                        event: Int
                    ) {
                        super.onDismissed(transientBottomBar, event)
                        this@MainActivity.updateQuestion()
                    }
                })
                .show()
        else
            Snackbar
                .make(this.mainActivity_view, R.string.incorrect_toast, Snackbar.LENGTH_SHORT)
                .setGestureInsetBottomIgnored(true)
                .show()

    }
}