package com.example.android.geoquiz


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel


class QuestionViewModel(
    private val state: SavedStateHandle,
) : ViewModel() {

    public var statement: MutableLiveData<Int> =
        state.getLiveData(question_key)

    public var answer: MutableLiveData<Int> =
        state.getLiveData(answer_key)

    private val questions =
        mapOf(
            R.string.question_statement_01 to R.bool.question_answer_01,
            R.string.question_statement_02 to R.bool.question_answer_02,
            R.string.question_statement_03 to R.bool.question_answer_03,
            R.string.question_statement_04 to R.bool.question_answer_04,
            R.string.question_statement_05 to R.bool.question_answer_05,
            R.string.question_statement_06 to R.bool.question_answer_06,
        )
            .toList()
            .listIterator()

    init {
        if (!state.contains(question_key))
            this.random()
    }

    public fun random() {
        this.questions
            .next()
            .also {
                state.set(question_key, it.first)
                state.set(answer_key, it.second)
            }
    }

    public fun getFirst(): Pair<Int, Int> =
        this.questions
            .next()

    public fun getNext(): Pair<Int, Int> =
        this.questions
            .next()

    companion object{

        private const val question_key = "question_res_id"
        private const val answer_key = "answer_res_id"

    }
}